package Modelo;

public class ModeloPrincipal {
    
    private String nombre, telefono, correo, celular;

    public ModeloPrincipal() {}

    public ModeloPrincipal(String nombre, String telefono, String correo, String celular) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo = correo;
        this.celular = celular;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
    
}
