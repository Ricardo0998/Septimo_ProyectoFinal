package Modelo;

import Singleton.Singleton;
import agenda_proyecto_final_barroso.Internet;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListaDAO extends DAO<ModeloLista> {

    public ListaDAO(Singleton conexion) {
        super(conexion);
    }

    @Override
    public void add(ModeloLista obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(ModeloLista obj) {
        if (Internet.getInternet()) {
            try {
                indice = 1;
                ps = conexion.obtenerConexion().prepareCall("update registros SET nombre = ?, telefono = ?, correo = ?, celular = ? where id = ?");
                ps.setString(indice++, obj.getNombre());
                ps.setString(indice++, obj.getTelefono());
                ps.setString(indice++, obj.getCorreo());
                ps.setString(indice++, obj.getCelular());
                ps.setInt(indice++, obj.getIdentificador());

                ps.execute();
            } catch (SQLException e) {
                System.out.println("Error ListaDAO: " + e.getMessage());
            }
        }else {
            System.out.println("Sin conexión a internet");
        }
    }

    @Override
    public void remove(ModeloLista obj) {
        if (Internet.getInternet()) {
            try {
                indice = 1;
                ps = conexion.obtenerConexion().prepareCall("delete from registros where id = ?");
                ps.setInt(indice++, obj.getIdentificador());

                ps.execute();
            } catch (SQLException e) {
                System.out.println("Error ListaDAO: " + e.getMessage());
            }
        }else {
            System.out.println("Sin conexión a internet");
        }
    }

    @Override
    public ResultSet consultar(String consulta) {
        try {
            ps = conexion.obtenerConexion().prepareStatement(consulta);
            rs = ps.executeQuery();
            return rs;
        } catch (SQLException e) {
            Logger.getLogger(PrincipalDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

}
