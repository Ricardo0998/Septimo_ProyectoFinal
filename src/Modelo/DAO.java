package Modelo;

import Singleton.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class DAO<T> {
    
    /*Variables de clase para poderlas extender en cada una de las clases  poder utilizarlos*/
    protected int indice;
    protected PreparedStatement ps;
    protected ResultSet rs;
    protected Singleton conexion;

    /*Constructor que inicializa la conexion a la base de datos*/
    public DAO(Singleton conexion) {
        this.conexion = conexion;
    }
    
    /*Metodos abstractos para poderlos usar en las clases en donde se implementen*/
    public abstract void add(T obj);
    public abstract void edit(T obj);
    public abstract void remove(T obj);
    public abstract ResultSet consultar(String consulta);
    
}
