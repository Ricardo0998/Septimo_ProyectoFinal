package Singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Singleton {
    
    /*Declaracion de variables de clase para la conexion a base de datos*/
    private static Singleton cnn;
    private final Connection connection;
    
    /*Constructor para realizar la conexion a el servidor de la base de datos*/
    private Singleton() throws ClassNotFoundException, SQLException{
        
        Class.forName("com.mysql.jdbc.Driver");
        
        connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/agenda",
                "root",
                ""
        );
        
    }
    
    /*Metodo donde se creara la instancia a la base de datos para solo obtener una sla instancia de la misma*/
    public static Singleton crearConexion() throws ClassNotFoundException, SQLException{
        
        if (cnn == null){
            cnn = new Singleton();
        }
        
        return cnn;
    }
    
    /*Metodod que regresa la conexion a la base de datos*/
    public Connection obtenerConexion(){
        return connection;
    }
    
}