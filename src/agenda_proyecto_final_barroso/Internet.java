package agenda_proyecto_final_barroso;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Internet {
    /*Metodo que regresara un verdadero si hay coneccion a internet y un false si es que no lo hay*/
    public static boolean getInternet() {
        try {
            URL u = new URL("https://www.google.com/");
            HttpsURLConnection huc = (HttpsURLConnection) u.openConnection();
            huc.connect();
            return true;
        } catch (Exception e) { 
            return false;
        }
    }
}
